(add-to-list 'load-path "~/.emacs.d/")

(package-initialize)

; package-install ecb ; emacs code browser ; все ломает
;; (require 'ecb)
;; (require 'ecb-autoloads)
;; (setq ecb-examples-bufferinfo-buffer-name nil)

; подсветка лисп выражений
;; (setq show-paren-style 'expression)
;; (show-paren-mode 2)

; отключение меню и тулбара
(menu-bar-mode +1)
(tool-bar-mode -1)
(delete-selection-mode 1)

;; word-wrap по emacs'освски
;; (visual-line-mode nil)
;; toggle-truncate-lines
;; (set-default 'truncate-lines t)
;; (global-visual-line-mode t)


; сохранение сессии
(desktop-save-mode 1)

(define-prefix-command 'mgs-map)
(global-set-key (kbd "C-k") 'mgs-map)

;; Путь к файлу в зеголовке окна
(setq frame-title-format
  '((:eval (if (buffer-file-name)
       (abbreviate-file-name (buffer-file-name)) "%b"))))


; https://github.com/nschum/highlight-symbol.el
(require 'highlight-symbol)
(global-set-key [(control f3)] 'highlight-symbol-at-point)
(global-set-key [f3] 'highlight-symbol-next)
(global-set-key [(shift f3)] 'highlight-symbol-prev)
(global-set-key [(meta f3)] 'highlight-symbol-query-replace)

(highlight-symbol-mode t)
(setq highlight-symbol-idle-delay 0.5)


(defun smart-beginning-of-line ()
  "Move point to first non-whitespace character or beginning-of-line.

Move point to the first non-whitespace character on this line.
If point was already at that position, move point to beginning of line."
  (interactive "^") ; Use (interactive "^") in Emacs 23 to make shift-select work
  (let ((oldpos (point)))
    (back-to-indentation)
    (and (= oldpos (point))
         (beginning-of-line))))
(put 'smart-beginning-of-line 'CUA 'move) ;; FIX: to enable shift - home select text
(global-set-key [home] 'smart-beginning-of-line)


;; (defun My-smart-home () "Odd home to beginning of line, even home to beginning of text/code."
;;    (interactive
;;     (if (and (eq last-command 'My-smart-home) (/= (line-beginning-position) (point)))
;;       (beginning-of-line)
;;       (beginning-of-line-text))
;;     )
;; )

;; (global-set-key [home] 'My-smart-home)


;; ERROR mouse double click broken minimap-move-overlay-mouse: Wrong type argument: overlayp, nil
;; (require 'minimap)
;; ;; (setq minimap-always-recenter nil)
;; (setq minimap-hide-scroll-bar t)
;; (setq minimap-recenter-type 'free)
;; (setq minimap-window-location 'right)

;;https://github.com/zenspider/enhanced-ruby-mode
(add-to-list 'load-path "~/.emacs.d/enhanced-ruby-mode-master")
(require 'enh-ruby-mode)
(autoload 'enh-ruby-mode "enh-ruby-mode" "Major mode for ruby files" t)
(add-to-list 'auto-mode-alist '("\\.rb$|\\.erb$" . enh-ruby-mode))
(add-to-list 'interpreter-mode-alist '("ruby" . enh-ruby-mode))



;; http://www.emacswiki.org/emacs/AutoComplete
(add-to-list 'load-path "~/.emacs.d/auto-complete")
(require 'auto-complete-config)
(ac-config-default)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/auto-complete/dict")
(define-key global-map (kbd "C-SPC") 'auto-complete)
;; (setq 'ac-trigger-key (kbd "C-SPC"))
(setq ac-source-yasnippet nil) ;; << FIX: ac-yasnippet-candidates: Wrong number of arguments: .....



;; dirty fix for having AC everywhere
(define-globalized-minor-mode real-global-auto-complete-mode
  auto-complete-mode (lambda ()
   (if (not (minibufferp (current-buffer)))
     (auto-complete-mode 1))
   ))
(real-global-auto-complete-mode t)


;; http://www.emacswiki.org/emacs/Yasnippet
(add-to-list 'load-path "~/.emacs.d/yasnippet")
(require 'yasnippet)
(yas-global-mode 1)
; (yas/load-directory "~/.emacs/yasnippet/snippets")
(setq yas-expand-from-trigger-key (kbd "C-SPC"))
(yas/load-directory "~/.emacs.d/elpa/yasnippet-20140929.240/snippets")
(defalias 'yas/get-snippet-tables 'yas--get-snippet-tables)
(defalias 'yas/table-hash 'yas--table-hash)


(defun ac-yasnippet-candidate-1 (table)
  (let ((hashtab (yas/snippet-table-hash table))
        (parent (if (fboundp 'yas/snippet-table-parent)
                    (car (yas/snippet-table-parent table))))
        candidates)
    (maphash (lambda (key value)
               (push key candidates))
             hashtab)
    (setq candidates (all-completions ac-prefix (nreverse candidates)))
    (if parent
        (setq candidates
              (append candidates (ac-yasnippet-candidate-1 parent))))
    candidates))


;; (setq pymacs-load-path '("/path/to/rope"
;;                          "/path/to/ropemacs"))
;; (add-to-list 'load-path "~/.emacs.d/Pymacs")

;; (add-to-list 'load-path "~/.emacs.d/el-get/pymacs")
;; (require 'pymacs)

;; (setq pymacs-load-path '("~/.emacs.d/el-get/rope/rope"
;;                          "~/.emacs.d/el-get/ropemacs/ropemacs"))

; (pymacs-load "ropemacs" "rope-")
; (pymacs-autoload 'ropemacs-mode "ropemacs" "rope-")
; (pymacs-load "ropemacs" "rope-")


;; обязательно после autocomplete and yasnipet
;;http://gabrielelanaro.github.io/emacs-for-python/
;; (load-file "~/.emacs.d/emacs-for-python/epy-init.el")
(add-to-list 'load-path "~/.emacs.d/emacs-for-python")
(require 'epy-setup)
(require 'epy-python)
(require 'epy-editing)
(require 'epy-completion)
(require 'epy-nose)
(require 'epy-bindings)


;; http://sourceforge.net/projects/emhacks/files/tabbar/1.3/
(require 'tabbar)
(tabbar-mode)
(global-unset-key [(control tab)])
(global-unset-key [(control shift tab)])
(global-unset-key [(control backtab)])
(global-unset-key (kbd "C-S-<iso-lefttab>"))
(global-set-key (kbd "C-<iso-lefttab>") 'tabbar-forward) ;; - переключение вперед
(global-set-key (kbd "C-S-<iso-lefttab>") 'tabbar-backward) ;; - переключение назад


;; http://code.google.com/p/dea/source/browse/trunk/my-lisps/linum%2B.el
(require 'linum+)
(setq linum-format "%d ")
(global-linum-mode 1)

;; built-in
(require 'ido)
(ido-mode t)
(setq ido-enable-flex-matching t)

(require 'projectile)
(projectile-global-mode)
;; (global-set-key (kbd "C-p") 'projectile-find-file-in-known-projects);слишком много
(global-set-key (kbd "C-p") 'helm-projectile)
(setq projectile-completion-system 'grizzl)

(add-to-list 'load-path "~/.emacs.d/grizzl")
(require 'grizzl)

;; built-in
(require 'bs)
(setq bs-configurations
      '(("files" "^\\*scratch\\*" nil nil bs-visits-non-file bs-sort-buffer-interns-are-last)))

(global-set-key (kbd "<f2>") 'bs-show)




;; http://www.emacswiki.org/emacs/SrSpeedbar
(require 'sr-speedbar)
(global-set-key (kbd "C-r") 'sr-speedbar-toggle)
(global-set-key (kbd "<f9>") 'sr-speedbar-toggle)
;; (global-set-key (kbd "\C-k b") 'sr-speedbar-toggle)
(define-key mgs-map (kbd "C-b") 'sr-speedbar-toggle)


(require 'multiple-cursors)
(define-key global-map (kbd "C-l") 'ignore)
(global-set-key (kbd "C-l") 'mc/edit-lines)
(global-set-key (kbd "C-d") 'mc/mark-next-like-this)
(global-set-key (kbd "C-S-d") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key [(meta shift up)] 'mc/mmlte--up)
(global-set-key [(meta shift down)] 'mc/mmlte--down)

(global-unset-key (kbd "C-<down-mouse-1>"))
(global-set-key (kbd "C-<mouse-1>") 'mc/add-cursor-on-click)


(require 'region-bindings-mode)
(region-bindings-mode-enable)
(define-key region-bindings-mode-map (kbd "<down-mouse-1>") 'mc/keyboard-quit)
(define-key region-bindings-mode-map (kbd "<escape>") 'mc/keyboard-quit)
(define-key region-bindings-mode-map "<down-mouse-1>" 'mc/keyboard-quit)
(define-key region-bindings-mode-map "p" 'mc/keyboard-quit)

(global-set-key (kbd "C-f") 'isearch-forward)
(global-set-key (kbd "C-s") 'save-buffer)

(global-set-key (kbd "C-o") 'find-file)

(global-set-key (kbd "C-z") 'undo)
(global-set-key (kbd "C-y") 'redo)

(global-set-key (kbd "C-b") 'list-buffers)

(global-set-key (kbd "C-c") 'yank)
(global-set-key (kbd "C-v") 'kill-ring-save)



(require 'highlight-parentheses)
(define-globalized-minor-mode global-highlight-parentheses-mode highlight-parentheses-mode
  (lambda nil (highlight-parentheses-mode t)))
(global-highlight-parentheses-mode t)

;; (defun lispy-parens ()
;;       "Setup parens display for lisp modes"
;;       (setq show-paren-delay 0)
;;       (setq show-paren-style 'parenthesis)
;;       (make-variable-buffer-local 'show-paren-mode)
;;       (show-paren-mode 1)
;;       (set-face-background 'show-paren-match-face (face-background 'default))
;;       (if (boundp 'font-lock-comment-face)
;;           (set-face-foreground 'show-paren-match-face
;;      			   (face-foreground 'font-lock-comment-face))
;;         (set-face-foreground 'show-paren-match-face
;;      			 (face-foreground 'default)))
;;       (set-face-attribute 'show-paren-match-face nil :weight 'extra-bold))



;(global-set-key (kbd "<M-down>") 'next-multiframe-window)
;; (global-set-key [(meta down)] 'next-multiframe-window)
;; (global-set-key [(meta up)] 'previous-multiframe-window)
(global-set-key [(meta down)] 'windmove-down)
(global-set-key [(meta up)] 'windmove-up)
(global-set-key [(meta left)] 'windmove-left)
(global-set-key [(meta right)] 'windmove-right)

;; resize windows
(global-set-key (kbd "C-s-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "C-s-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "C-s-<down>") 'shrink-window)
(global-set-key (kbd "C-s-<up>") 'enlarge-window)


(define-key global-map (kbd "C-w") 'ignore)
;; (global-set-key (kbd "C-w") 'kill-window)
(global-set-key (kbd "C-w") 'delete-window)
(global-set-key (kbd "M-w") 'kill-buffer)

(global-set-key (kbd "C-/") 'comment-or-uncomment-region)

(global-set-key (kbd "<C-tab>") 'next-multiframe-window)
(global-set-key (kbd "<C-S-tab>") 'previous-multiframe-window)
(global-set-key (kbd "<C-S-iso-lefttab>") 'previous-multiframe-window)





; (global-set-key (kbd "<C-f2>") 'set-mark-command)
; (global-set-key (kbd "<f2>") 'exchange-point-and-mark)

; (setq default-input-method "cyrillic-jcuken")



(loop
 for from across "йцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖ\ЭЯЧСМИТЬБЮ№"
 for to   across "qwertyuiop[]asdfghjkl;'zxcvbnm,.QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>#"
 do
 (eval `(define-key key-translation-map (kbd ,(concat "C-" (string from))) (kbd ,(concat     "C-" (string to)))))
 (eval `(define-key key-translation-map (kbd ,(concat "M-" (string from))) (kbd ,(concat     "M-" (string to))))))

; палка на пустых строчках
;(defun cursor-shape-hook ()
;    (if (equal (thing-at-point 'line) "\n") (setq cursor-type 'bar)
;       (setq cursor-type 'box)))
;(add-hook 'post-command-hook 'cursor-shape-hook)

; чтобы курсор был в виде палки
(setq-default cursor-type 'bar)

;чтобы измененные файлы подгружались автоматом
(global-auto-revert-mode t)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-enabled-themes (quote (monokai)))
 '(custom-safe-themes (quote ("bd115791a5ac6058164193164fd1245ac9dc97207783eae036f0bfc9ad9670e0" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default)))
 '(highlight-symbol-foreground-color "#93a1a1")
 '(minimap-update-delay 1)
 '(package-archives (quote (("gnu" . "http://elpa.gnu.org/packages/") ("melpa" . "http://melpa.milkbox.net/packages/"))))
 '(tabbar-separator (quote (" | ")))
 '(term-default-bg-color "#002b36")
 '(term-default-fg-color "#839496"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#272822" :foreground "#F8F8F2" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 100 :width normal :foundry "unknown" :family "DejaVu Sans Mono"))))
 '(minimap-semantic-function-face ((t (:inherit (font-lock-function-name-face minimap-font-face) :background "gray10" :box (:line-width 1 :color "white") :height 2.9 :width extra-expanded))))
 '(tabbar-default-face ((t (:inherit variable-pitch :background "gray72" :foreground "black" :height 0.8)))))



(require 'paren)
(setq show-paren-delay 0)
(show-paren-mode t)                 ; turn paren-mode on
(make-variable-buffer-local 'show-paren-mode)
(set-face-background 'show-paren-match (face-background 'default))
(set-face-foreground 'show-paren-match "#272822")
(set-face-background 'show-paren-match "#fe9044")
(set-face-attribute 'show-paren-match nil :weight 'extra-bold)

;; (defun lispy-parens ()
;;       "Setup parens display for lisp modes"
;;       (setq show-paren-delay 0)
;;       (setq show-paren-style 'parenthesis)
;;       (make-variable-buffer-local 'show-paren-mode)
;;       (show-paren-mode 1)
;;       (set-face-background 'show-paren-match-face (face-background 'default))
;;       (if (boundp 'font-lock-comment-face)
;;           (set-face-foreground 'show-paren-match-face
;;      			   (face-foreground 'font-lock-comment-face))
;;         (set-face-foreground 'show-paren-match-face
;;      			 (face-foreground 'default)))
;;       (set-face-attribute 'show-paren-match-face nil :weight 'extra-bold))


;; goto-matching-paren
;; -------------------
;; If point is sitting on a parenthetic character, jump to its match.
;; This matches the standard parenthesis highlighting for determining which
;; one it is sitting on.
;;
(defun goto-matching-paren ()
  "If point is sitting on a parenthetic character, jump to its match."
  (interactive)
  (cond ((looking-at "\\s\(") (forward-list 1))
        ((progn
           (backward-char 1)
           (looking-at "\\s\)")) (forward-char 1) (backward-list 1))))
(define-key global-map [(control ?k) (control ?p)] 'goto-matching-paren) ; Bind to C-k C-p



(defun xah-clean-whitespace (p1 p2)
  "Delete trailing whitespace, and replace sequence of newlines into just 2.
Work on text selection or whole buffer."
  (interactive
   (if (region-active-p)
       (list (region-beginning) (region-end))
     (list 1 (point-max))))
  (save-excursion
    (save-restriction
      (narrow-to-region p1 p2)
      (progn
        (goto-char (point-min))
        (while (search-forward-regexp "[ \t]+\n" nil "noerror")
          (replace-match "\n")))
      (progn
        (goto-char (point-min))
        (while (search-forward-regexp "[\t]" nil "noerror")
          (replace-match "    ")))
      (progn
        (goto-char (point-min))
        (while (search-forward-regexp "\n\n\n+" nil "noerror")
          (replace-match "\n\n"))))))

(add-hook 'before-save-hook 'delete-trailing-whitespace)



;; (setq animals '(gazelle giraffe lion tiger))
;; (defun print-elements-of-list (list)
;;        "Print each element of LIST on a line of its own."
;;        (while list
;;          (print (car list))
;;          (Setq list (cdr list))))

;; (print-elements-of-list animals)
;; (print-elements-of-list load-path)


;;;;;;;;;;;;;;;;;;;MY BINDS

;; (global-set-key [C-S-right] 'shift-right)
;; (global-set-key [C-S-left] 'shift-left)
(global-set-key (kbd "TAB") 'shift-right)
(global-set-key (kbd "<backtab>") 'shift-left)
;; (global-set-key (kbd "<backtab>") 'indent-region)

(cua-mode t)
(setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands
(transient-mark-mode 1) ;; No region when it is not highlighted
(setq cua-keep-region-after-copy t) ;; Standard Windows behaviour

 ;; shift + click select region
 (define-key global-map (kbd "<S-down-mouse-1>") 'ignore) ; turn off font dialog
 (define-key global-map (kbd "<S-mouse-1>") 'mouse-set-point)
 (put 'mouse-set-point 'CUA 'move)

; включаем возможность делать Uppercase и Downcase для выделенного текста
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;; (define-key mgs-map (kbd "C-d") 'command)
(define-key mgs-map (kbd "C-d") 'command)
(define-key mgs-map (kbd "C-u") 'upcase-region)
(define-key mgs-map (kbd "C-l") 'downcase-region)
(define-key mgs-map (kbd "C-c") 'capitalize-region)
(define-key mgs-map (kbd "C-t") 'toggle-truncate-lines)
(define-key mgs-map (kbd "C-g") 'goto-line)


;; Shift the selected region right if distance is postive, left if
;; negative

(defun shift-region (distance)
  (let ((mark (mark)))
    (save-excursion
      (indent-rigidly (region-beginning) (region-end) distance)
      (push-mark mark t t)
      ;; Tell the command loop not to deactivate the mark
      ;; for transient mark mode
      (setq deactivate-mark nil))))

(defun shift-right ()
  (interactive)
  (shift-region 4))

(defun shift-left ()
  (interactive)
  (shift-region -4))

;; Bind (shift-right) and (shift-left) function to your favorite keys. I use
;; the following so that Ctrl-Shift-Right Arrow moves selected text one
;; column to the right, Ctrl-Shift-Left Arrow moves selected text one
;; column to the left:


;;; Indentation for python

;; Ignoring electric indentation
(defun electric-indent-ignore-python (char)
  "Ignore electric indentation for python-mode"
  (if (equal major-mode 'python-mode)
      `no-indent'
    nil))
(add-hook 'electric-indent-functions 'electric-indent-ignore-python)

;; чтобы питон-моде не портил бинд на Shift-tab
(add-hook 'python-mode-hook
  (lambda()
    (local-unset-key (kbd "<backtab>"))))

;; Enter key executes newline-and-indent
(defun set-newline-and-indent ()
  "Map the return key with `newline-and-indent'"
  (local-set-key (kbd "RET") 'newline-and-indent))
(add-hook 'python-mode-hook 'set-newline-and-indent)
