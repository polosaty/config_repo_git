;;; minimap-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (minimap-kill minimap-create minimap-toggle) "minimap"
;;;;;;  "../../../../../../.emacs.d/elpa/minimap-20140201.1209/minimap.el"
;;;;;;  "afec4c99532eed5f1134dca7078b9eeb")
;;; Generated autoloads from ../../../../../../.emacs.d/elpa/minimap-20140201.1209/minimap.el

(autoload 'minimap-toggle "minimap" "\
Toggle the minimap.

\(fn)" t nil)

(autoload 'minimap-create "minimap" "\
Create the minimap sidebar

\(fn)" t nil)

(autoload 'minimap-kill "minimap" "\
Kill minimap for current buffer.
Cancel the idle timer if no more minimaps are active.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("../../../../../../.emacs.d/elpa/minimap-20140201.1209/minimap-pkg.el"
;;;;;;  "../../../../../../.emacs.d/elpa/minimap-20140201.1209/minimap.el")
;;;;;;  (21559 44996 877444 469000))

;;;***

(provide 'minimap-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; minimap-autoloads.el ends here
