;;; monokai-theme-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "monokai-theme" "monokai-theme.el" (21529 22172
;;;;;;  829801 840000))
;;; Generated autoloads from monokai-theme.el

(when (and (boundp 'custom-theme-load-path) load-file-name) (add-to-list 'custom-theme-load-path (file-name-as-directory (file-name-directory load-file-name))))

;;;***

;;;### (autoloads nil nil ("monokai-theme-pkg.el") (21529 22172 939337
;;;;;;  219000))

;;;***

(provide 'monokai-theme-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; monokai-theme-autoloads.el ends here
