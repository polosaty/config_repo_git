ln -sf `pwd`/lubuntu-rc.xml ~/.config/openbox/lubuntu-rc.xml
ls --color -la ~/.config/openbox/lubuntu-rc.xml

ln -sf `pwd`/autostart ~/.config/lxsession/Lubuntu/autostart
ls --color -la ~/.config/lxsession/Lubuntu/autostart

ln -sf `pwd`/translate.py ~/bin/translate.py
ls --color -la ~/bin/translate.py
