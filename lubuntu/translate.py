#!/usr/bin/python3

s1 = '''qwertyuiop[]asdfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:"ZXCVBNM<>?`~!@#$%^&*()_+|'''
s2 = '''йцукенгшщзхъфывапролджэячсмитьбю.ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,ёЁ!"№;%:?*()_+/'''


def tr_(src, keystr_src, keystr_dst):
    dst = ''
    for i in src:
        try:
            indx = keystr_src.index(i)
            dst += keystr_dst[indx]
        except:
            dst += i
    return dst


def tr(src):
    dst = ''
    for i in src:
        indx = None
        try:
            # print(1,i)
            indx = s1.index(i)
            dst += s2[indx]
            # print(dst)
            continue
        except:
            pass

        try:
            # print(2,i)
            indx = s2.index(i)
            dst += s1[indx]
            # print(dst)
            continue
        except:
            pass

        dst += i

    return dst


def main():
    # print(tr(input()))
    src = input()
    if not src:
        return ""

    str1 = tr_(src, s1, s2)
    str2 = tr_(src, s2, s1)

    # diff1 = sum([src[i] != j for i, j in enumerate(str1)])
    # diff2 = sum([src[i] != j for i, j in enumerate(str2)])
    diff1 = diff2 = 0
    for i, j in enumerate(src):
        diff1 += str1[i] != j
        diff2 += str2[i] != j

    # import sys
    # print('src :', src, file=sys.stderr)
    # print('str1:', str1, file=sys.stderr)
    # print('str2:', str2, file=sys.stderr)
    # print('diff1:', diff1, file=sys.stderr)
    # print('diff2:', diff2, file=sys.stderr)
    # print('result:', (str1 if diff1 > diff2 else str2), file=sys.stderr, flush=True)

    print((str1 if diff1 > diff2 else str2), end="", flush=True)

if __name__ == '__main__':
    main()
