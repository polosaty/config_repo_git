#!/bin/bash

cb=$(xclip -o -selection clipboard)

echo $cb >> /tmp/log

xsel -o | ~/bin/translate | xclip -i -selection clipboard

sleep 0.3
xdotool key "ctrl+v"
sleep 0.3

xclip -o -selection clipboard >> /tmp/log
echo -ne $cb | xclip -i -selection clipboard

